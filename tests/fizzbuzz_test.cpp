#include "fizzbuzz/fizzbuzz.h"
#include "acutest/acutest.h"

void test_Fizzbuzz_decode(void)
{
    Fizzbuzz fizzbuzz;

	TEST_CHECK(fizzbuzz.decode(0) == std::string("0"));
    TEST_CHECK(fizzbuzz.decode(1) == std::string("1"));
    TEST_CHECK(fizzbuzz.decode(3) == std::string("fizz"));
}

TEST_LIST = {
   { "Fizzbuzz testing decode division by three and replaced by fizz", test_Fizzbuzz_decode },
   { NULL, NULL }
};