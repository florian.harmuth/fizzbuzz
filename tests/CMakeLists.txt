include_directories(${CMAKE_SOURCE_DIR}/extern)

# Tests need to be added as executables first
add_executable(fizzbuzz_test fizzbuzz_test.cpp ${CMAKE_SOURCE_DIR}/include/fizzbuzz/fizzbuzz.h ${CMAKE_SOURCE_DIR}/extern/acutest/acutest.h)