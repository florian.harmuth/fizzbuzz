#include <string>

class Fizzbuzz
{
    public:
        std::string decode(int number) 
        {
            if ((number % 3) == 0 && number != 0)
            {
                return std::string("fizz");
            }
            else 
            {
                return std::to_string(number);
            }
        };
};